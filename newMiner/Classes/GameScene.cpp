#include "GameScene.h"
#include <cstdlib>
#include <ctime>

USING_NS_CC;

Scene* GameScene::createScene()
{
	auto newScene = GameScene::create();
    
    
    
    return newScene;
}

// on "init" you need to initialize your instance
bool GameScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Scene::init())
	{
		return false;
	}

    auto stoneLayer = Layer::create();
    auto mouseLayer = Layer::create();
    
	//add touch listener
	EventListenerTouchOneByOne* listener = EventListenerTouchOneByOne::create();
	listener->setSwallowTouches(true);
	listener->onTouchBegan = CC_CALLBACK_2(GameScene::onTouchBegan, this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);


	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
    auto winSize = Director::getInstance()->getWinSize();
    
    mouseLayer->setAnchorPoint(Vec2(0, visibleSize.height / 2));
    mouseLayer->setPosition(Vec2(0, visibleSize.height / 2));
    
    auto background = Sprite::create("level-background-0.jpg");
    auto bgTexture = background->getTextureRect();
    background->setScale(winSize.width / bgTexture.getMaxX(), winSize.height / bgTexture.getMaxY());
    background->setPosition(Vec2(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 2));
    this->addChild(background, 0);
    
    auto stationRock = Sprite::create("stone.png");
    stationRock->setPosition(Vec2(560, 480));
    auto animate = Animate::create(AnimationCache::getInstance()->getAnimation("stoneAnimation"));
    stationRock->runAction(RepeatForever::create(animate));
    stone = stationRock;
    
    stoneLayer->addChild(stationRock, 1);
    
    auto shootMenuItem = MenuItemFont::create("Shoot",
                                               CC_CALLBACK_1(GameScene::shootMenuCallback, this));
    shootMenuItem->setPosition(Vec2(760, 480));
    shootMenuItem->setFontSizeObj(50);
    auto menu = Menu::create(shootMenuItem, NULL);
    menu->setPosition(Vec2::ZERO);
    
    stoneLayer->addChild(menu, 1);
    
    mouse = Sprite::createWithSpriteFrameName("mouse-0.png");
    animate = Animate::create(AnimationCache::getInstance()->getAnimation("gemMouseAnimation"));
    mouse->runAction(RepeatForever::create(animate));
    mouse->setName("Mouse");
    mouse->setPosition(Vec2(visibleSize.width / 2, 0));
    mouseLayer->addChild(mouse, 1);

    
    this->addChild(stoneLayer, 0);
    this->addChild(mouseLayer, 1);
    this->mouseLayer = mouseLayer;
    this->stoneLayer = stoneLayer;
	return true;
}

bool GameScene::onTouchBegan(Touch *touch, Event *unused_event) {

	auto location = touch->getLocation();
    
    if (location.y > SOIL_HEIGHT) return false;

    auto newCheese = Sprite::create("cheese.png");
    newCheese->setPosition(location);
    auto fadeout = FadeOut::create(5);
    newCheese->runAction(fadeout);
    this->addChild(newCheese, 1);
    
    location.set(location.x, location.y - mouseLayer->getPosition().y);
    
    auto moveToCheese = MoveTo::create(1.5, location);
    mouse->runAction(moveToCheese);
    
	return true;
}

void GameScene::shootMenuCallback(cocos2d::Ref *pSender) {
    auto shootingRock = Sprite::create("stone.png");
    shootingRock->setPosition(Vec2(560, 480));
    auto mouseLocation = mouse->getPosition();
    mouseLocation.set(mouseLocation.x, mouseLocation.y + mouseLayer->getPosition().y);
    
    auto moveToMouse = MoveTo::create(1.5, mouseLocation);
    auto fadeout = FadeOut::create(1);
    auto seq = Sequence::create(moveToMouse, fadeout, NULL);
    shootingRock->runAction(seq);
    
    int x = rand() % 960;
    int y = rand() % 320;
    
    auto moveToRandomLocation = MoveTo::create(2, Vec2(x, y - mouseLayer->getPosition().y));
    mouse->runAction(moveToRandomLocation);
    
    auto diamond = Sprite::create("diamond.png");
    auto diamondAnimate = Animate::create(AnimationCache::getInstance()->getAnimation("diamondAnimation"));
    diamond->runAction(RepeatForever::create(diamondAnimate));

    diamond->setPosition(mouseLocation);
    stoneLayer->addChild(diamond, 1);
    
    stoneLayer->addChild(shootingRock, 1);
}
