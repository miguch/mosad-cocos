#pragma once
#include "cocos2d.h"
using namespace cocos2d;

class HelloWorld : public cocos2d::Scene
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init();
        
    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);
private:
	cocos2d::Sprite* player;
	cocos2d::Vector<SpriteFrame*> attack;
	cocos2d::Vector<SpriteFrame*> dead;
	cocos2d::Vector<SpriteFrame*> run;
	cocos2d::Vector<SpriteFrame*> idle;
	cocos2d::Size visibleSize;
	cocos2d::Vec2 origin;
	cocos2d::Label* time;
    cocos2d::Label* score;
    cocos2d::Label* maxScore;
    int score_num;
    int max_score_num;
	int dtime;
    bool isPlayerDead;
	cocos2d::ProgressTimer* pT;
    void updateCustom(float dt);
    void checkCollision(float dt);
    void monsterMove(float dt);
    void addScore();
    void playerInitImage();
    void movement_onClick(cocos2d::Ref *pSender, char dir);
};
