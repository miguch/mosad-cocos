#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"
USING_NS_CC;

Scene* HelloWorld::createScene()
{
    return HelloWorld::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}


// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

    // add a "close" icon to exit the progress. it's an autorelease object
    auto clickItem = MenuItemImage::create(
                                           "buttonIco.png",
                                           "buttonIco.png",
                                           CC_CALLBACK_1(HelloWorld::menuCloseCallback, this));

    if (clickItem == nullptr ||
        clickItem->getContentSize().width <= 0 ||
        clickItem->getContentSize().height <= 0)
    {
        problemLoading("'buttonIco.png' and 'buttonIco.png'");
    }
    else
    {
        float x = origin.x + visibleSize.width - clickItem->getContentSize().width/2;
        float y = origin.y + clickItem->getContentSize().height/2;
        clickItem->setPosition(Vec2(x,y));
    }

    // create menu, it's an autorelease object
    auto menu = Menu::create(clickItem, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);
    
    auto closeItem = MenuItemImage::create(
                                           "CloseNormal.png",
                                           "CloseSelected.png",
                                           CC_CALLBACK_1(HelloWorld::exitCallback, this));
    
    if (closeItem == nullptr ||
        closeItem->getContentSize().width <= 0 ||
        closeItem->getContentSize().height <= 0)
    {
        problemLoading("'CloseNormal.png' and 'CloseSelected.png'");
    }
    else
    {
        float x = origin.x + visibleSize.width - closeItem->getContentSize().width/2;
        float y = origin.y + closeItem->getContentSize().height/2 + clickItem->getContentSize().height * 2;
        closeItem->setPosition(Vec2(x,y));
    }
    
    // create menu, it's an autorelease object
    auto exitMenu = Menu::create(closeItem, NULL);
    exitMenu->setPosition(Vec2::ZERO);
    this->addChild(exitMenu, 1);

    /////////////////////////////
    // 3. add your codes below...

    // add a label shows name and sid
    // create and initialize a label

//    CCDictionary *strings = CCDictionary::createWithContentsOfFileThreadSafe("String.xml");
    ValueMap strings = FileUtils::getInstance()->getValueMapFromFile("String.xml");
//    const char* name = ((CCString*)strings->objectForKey("Name"))->getCString();
//    const char* sid = ((CCString*)strings->objectForKey("Sid"))->getCString();
    
    auto a = strings["Name"].asString();
    auto b = strings["Sid"].asString();
    const char* name = a.c_str();
    const char* sid = b.c_str();
    
//    const char* name = "陈铭涛";
//    const char* sid = "16340024";
    
    auto nameLabel = Label::createWithSystemFont(name, "Heiti SC", 24);
    if (nameLabel == nullptr)
    {
        problemLoading("'Heiti SC'");
    }
    else
    {
        // position the label on the center of the screen
        nameLabel->setPosition(Vec2(origin.x + visibleSize.width/2,
                                origin.y + visibleSize.height - nameLabel->getContentSize().height));
        nameLabel->setName("Sname");

        // add the label as a child to this layer
        this->addChild(nameLabel, 1);
    }

    auto sidLabel = Label::createWithSystemFont(sid, "PingFang SC", 22);
    if (sidLabel == nullptr)
    {
        problemLoading("'PingFang SC'");
    }
    else
    {
        // position the label on the center of the screen
        sidLabel->setPosition(Vec2(origin.x + visibleSize.width / 2,
            origin.y + visibleSize.height - sidLabel->getContentSize().height 
            - nameLabel->getContentSize().height));
        sidLabel->setName("Sid");

        // add the label as a child to this layer
        this->addChild(sidLabel, 1);
    }

    // add "HelloWorld" splash screen"
    auto sprite = Sprite::create("mig.jpg");
    if (sprite == nullptr)
    {
        problemLoading("'mig.jpg'");
    }
    else
    {
        // position the sprite on the center of the screen
        sprite->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
        sprite->setName("MainPic");

        // add the sprite as a child to this layer
        this->addChild(sprite, 0);
    }
    return true;
}

void HelloWorld::exitCallback(cocos2d::Ref *pSender) {
    Director::getInstance()->end();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}

void HelloWorld::menuCloseCallback(Ref* pSender)
{
    auto pic = (Sprite*)(this->getChildByName("MainPic"));
    auto nameLabel = (Label*)(this->getChildByName("Sname"));
    auto sidLable = (Label*)(this->getChildByName("Sid"));
    switch (menuClickCount) {
    case 0:
        pic->setTexture("198.png");
        break;
    case 1:
        nameLabel->setColor(Color3B::RED);
        break;
    case 2:
        sidLable->setColor(Color3B::BLUE);
        break;
    case 3:
        MessageBox("Clicked!", "~~");
        break;
    case 4:
        pic->setTexture("mig.jpg");
        sidLable->setColor(Color3B::WHITE);
        nameLabel->setColor(Color3B::WHITE);
        break;
    default:
        Director::getInstance()->end();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
        exit(0);
#endif
    }
    menuClickCount++;

    //Close the cocos2d-x game scene and quit the application

    
    

//    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
//    exit(0);
//#endif

    /*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() and exit(0) as given above,instead trigger a custom event created in RootViewController.mm as below*/

    //EventCustom customEndEvent("game_scene_close_event");
    //_eventDispatcher->dispatchEvent(&customEndEvent);

}
