#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"
#pragma execution_character_set("utf-8")

USING_NS_CC;

Scene* HelloWorld::createScene()
{
    return HelloWorld::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }

    visibleSize = Director::getInstance()->getVisibleSize();
    origin = Director::getInstance()->getVisibleOrigin();

	//创建一张贴图
	auto texture = Director::getInstance()->getTextureCache()->addImage("$lucia_2.png");
	//从贴图中以像素单位切割，创建关键帧
	auto frame0 = SpriteFrame::createWithTexture(texture, CC_RECT_PIXELS_TO_POINTS(Rect(0, 0, 113, 113)));
	//使用第一帧创建精灵
	player = Sprite::createWithSpriteFrame(frame0);
	player->setPosition(Vec2(origin.x + visibleSize.width / 2,
		origin.y + visibleSize.height / 2));
	addChild(player, 3);

	//hp条
	Sprite* sp0 = Sprite::create("hp.png", CC_RECT_PIXELS_TO_POINTS(Rect(0, 320, 420, 47)));
	Sprite* sp = Sprite::create("hp.png", CC_RECT_PIXELS_TO_POINTS(Rect(610, 362, 4, 16)));

	//使用hp条设置progressBar
	pT = ProgressTimer::create(sp);
	pT->setScaleX(90);
	pT->setAnchorPoint(Vec2(0, 0));
	pT->setType(ProgressTimerType::BAR);
	pT->setBarChangeRate(Point(1, 0));
	pT->setMidpoint(Point(0, 1));
	pT->setPercentage(100);
	pT->setPosition(Vec2(origin.x + 14 * pT->getContentSize().width, origin.y + visibleSize.height - 2 * pT->getContentSize().height));
	addChild(pT, 1);
	sp0->setAnchorPoint(Vec2(0, 0));
	sp0->setPosition(Vec2(origin.x + pT->getContentSize().width, origin.y + visibleSize.height - sp0->getContentSize().height));
	addChild(sp0, 0);

	// 静态动画
	idle.reserve(1);
	idle.pushBack(frame0);

	// 攻击动画
	attack.reserve(17);
	for (int i = 0; i < 17; i++) {
		auto frame = SpriteFrame::createWithTexture(texture, CC_RECT_PIXELS_TO_POINTS(Rect(113 * i, 0, 113, 113)));
		attack.pushBack(frame);
	}

	// 可以仿照攻击动画
	// 死亡动画(帧数：22帧，高：90，宽：79）
    auto deadTexture = Director::getInstance()->getTextureCache()->addImage("$lucia_dead.png");
    dead.reserve(22);
    for (int i = 0; i < 22; i++) {
        auto frame = SpriteFrame::createWithTexture(deadTexture, CC_RECT_PIXELS_TO_POINTS(Rect(79 * i, 0, 70, 90)));
        dead.pushBack(frame);
    }

	// 运动动画(帧数：8帧，高：101，宽：68）
	auto forwardTexture = Director::getInstance()->getTextureCache()->addImage("$lucia_forward.png");
    run.reserve(8);
    for (int i = 0; i < 8; i++) {
        auto frame = SpriteFrame::createWithTexture(forwardTexture, CC_RECT_PIXELS_TO_POINTS(Rect(68 * i, 0, 68, 101)));
        run.pushBack(frame);
    }
    
    time = Label::createWithTTF("180", "fonts/arial.ttf", 36);
    time->setPosition(Size(origin.x + visibleSize.width / 2, origin.y + visibleSize.height * 0.9));
    addChild(time, 1);
    
    auto wLabel = Label::createWithTTF("W", "fonts/arial.ttf", 36);
    auto wMenuItem = MenuItemLabel::create(wLabel, [&](cocos2d::Ref *pSender) {
        auto playerActNum = player->getNumberOfRunningActions();
        if (playerActNum == 0) {
            auto animation = Animation::createWithSpriteFrames(run, 0.1f);
            auto runAnimate = Animate::create(animation);
            auto currentLoc = player->getPosition();
            auto initImage = CallFunc::create(CC_CALLBACK_0(HelloWorld::playerInitImage, this));
            if (currentLoc.y + (visibleSize.height / 16) < visibleSize.height - 60) {
                auto move = MoveBy::create(0.8f, Vec2(0, visibleSize.height / 16));
                auto movement = Spawn::create(runAnimate, move, NULL);
                player->runAction(Sequence::create(movement, initImage, NULL));
            }
        }
    });
    auto wMenu = Menu::create(wMenuItem, NULL);
    wMenu->setPosition(Vec2(80, 100));
    addChild(wMenu, 1);
    
    auto sLabel = Label::createWithTTF("S", "fonts/arial.ttf", 36);
    auto sMenuItem = MenuItemLabel::create(sLabel, [&](cocos2d::Ref *pSender) {
        auto playerActNum = player->getNumberOfRunningActions();
        if (playerActNum == 0) {
            auto animation = Animation::createWithSpriteFrames(run, 0.1f);
            auto runAnimate = Animate::create(animation);
            auto currentLoc = player->getPosition();
            auto initImage = CallFunc::create(CC_CALLBACK_0(HelloWorld::playerInitImage, this));
            if (currentLoc.y - (visibleSize.height / 16) > 0) {
                auto move = MoveBy::create(0.8f, Vec2(0, -visibleSize.height / 16));
                auto movement = Spawn::create(runAnimate, move, NULL);
                player->runAction(Sequence::create(movement, initImage, NULL));
            }
        }
    });
    auto sMenu = Menu::create(sMenuItem, NULL);
    sMenu->setPosition(Vec2(80, 50));
    addChild(sMenu, 1);
    
    auto aLabel = Label::createWithTTF("A", "fonts/arial.ttf", 36);
    auto aMenuItem = MenuItemLabel::create(aLabel, [&](cocos2d::Ref *pSender) {
        auto playerActNum = player->getNumberOfRunningActions();
        if (playerActNum == 0) {
            auto animation = Animation::createWithSpriteFrames(run, 0.1f);
            auto runAnimate = Animate::create(animation);
            auto currentLoc = player->getPosition();
            auto initImage = CallFunc::create(CC_CALLBACK_0(HelloWorld::playerInitImage, this));
            if (currentLoc.x - (visibleSize.width / 16) > 0) {
                auto move = MoveBy::create(0.8f, Vec2(-visibleSize.width / 16, 0));
                auto movement = Spawn::create(runAnimate, move, NULL);
                player->runAction(Sequence::create(movement, initImage, NULL));
            }
        }
    });
    auto aMenu = Menu::create(aMenuItem, NULL);
    aMenu->setPosition(Vec2(30, 50));
    addChild(aMenu, 1);
    
    auto dLabel = Label::createWithTTF("D", "fonts/arial.ttf", 36);
    auto dMenuItem = MenuItemLabel::create(dLabel, [&](cocos2d::Ref *pSender) {
        auto playerActNum = player->getNumberOfRunningActions();
        if (playerActNum == 0) {
            auto animation = Animation::createWithSpriteFrames(run, 0.1f);
            auto runAnimate = Animate::create(animation);
            auto currentLoc = player->getPosition();
            auto initImage = CallFunc::create(CC_CALLBACK_0(HelloWorld::playerInitImage, this));
            if (currentLoc.x + (visibleSize.width / 16) < visibleSize.width) {
                auto move = MoveBy::create(0.8f, Vec2(visibleSize.width / 16, 0));
                auto movement = Spawn::create(runAnimate, move, NULL);
                player->runAction(Sequence::create(movement, initImage, NULL));
            }
        }
    });
    
    auto dMenu = Menu::create(dMenuItem, NULL);
    dMenu->setPosition(Vec2(130, 50));
    addChild(dMenu, 1);
    
    
    auto xLabel = Label::createWithTTF("X", "fonts/arial.ttf", 36);
    auto xMenuItem = MenuItemLabel::create(xLabel, [&](cocos2d::Ref *pSender) {
        auto playerActNum = player->getNumberOfRunningActions();
        if (playerActNum == 0) {
            auto animation = Animation::createWithSpriteFrames(dead, 0.1f);
            auto deadAnimate = Animate::create(animation);
            auto initImage = CallFunc::create(CC_CALLBACK_0(HelloWorld::playerInitImage, this));
            player->runAction(Sequence::create(deadAnimate, initImage, NULL));
            if (pT->getPercentage() - 20 >= 0) {
                pT->runAction(ProgressTo::create(1.0f, pT->getPercentage() - 20));
            }
        }
    });
    
    auto xMenu = Menu::create(xMenuItem, NULL);
    xMenu->setPosition(Vec2(680, 100));
    addChild(xMenu, 1);
    
    auto yLabel = Label::createWithTTF("Y", "fonts/arial.ttf", 36);
    auto yMenuItem = MenuItemLabel::create(yLabel, [&](cocos2d::Ref *pSender) {
        auto playerActNum = player->getNumberOfRunningActions();
        if (playerActNum == 0) {
            auto animation = Animation::createWithSpriteFrames(attack, 0.1f);
            auto attackAnimate = Animate::create(animation);
            auto initImage = CallFunc::create(CC_CALLBACK_0(HelloWorld::playerInitImage, this));
            player->runAction(Sequence::create(attackAnimate, initImage, NULL));
            if (pT->getPercentage() + 20 <= 100) {
                pT->runAction(ProgressTo::create(1.0f, pT->getPercentage() + 20));
            }
        }
    });
    
    auto yMenu = Menu::create(yMenuItem, NULL);
    yMenu->setPosition(Vec2(650, 50));
    addChild(yMenu, 1);
    
    schedule(schedule_selector(HelloWorld::updateCustom), 1.0f, kRepeatForever, 0);

    return true;
}

void HelloWorld::updateCustom(float dt) {
    int current = atoi(time->getString().c_str());
    if (current != 0)
        current--;
    time->setString(std::to_string(current));
}

void HelloWorld::playerInitImage() {
    auto texture = Director::getInstance()->getTextureCache()->addImage("$lucia_dead.png");
    auto frame0 = SpriteFrame::createWithTexture(texture, CC_RECT_PIXELS_TO_POINTS(Rect(0, 0, 80, 90)));
    player->setSpriteFrame(frame0);
}

